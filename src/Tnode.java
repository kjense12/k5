import java.lang.reflect.Array;
import java.util.*;

/**
 * Class Tnode represents a tree of nodes.
 *
 * References: https://enos.itcollege.ee/~jpoial/algoritmid/puud.html,
 * https://enos.itcollege.ee/~jpoial/algoritmid/puustruktuurid.html,
 * https://www.hackerearth.com/practice/notes/iterative-tree-traversals/
 */

public class Tnode{

   private String name;
   private Tnode firstChild;
   private Tnode rightSibling;
   private int info;

   /**
    * Constructor.
    *
    * @param s - String representation of a node.
    * @param sibling - Node that is also related to root.
    * @param child - Node that this node is root to.
    */
   Tnode (String s, Tnode sibling, Tnode child) {
      setName (s);
      setRightSibling(sibling);
      setFirstChild (child);
   }

   /**
    * Set name of node.
    *
    * @param s - String representation of a node.
    */
   private void setName(String s) { name = s; }

   /**
    * Get name of node.
    *
    * @return String representation of a node
    */
   public String getName() { return name; }

   /**
    * Set first child.
    *
    * @param child - Node to set as child.
    */
   private void setFirstChild (Tnode child) { firstChild = child; }

   /**
    * Get first child.
    *
    * @return - Node that is set as firstChild.
    */
   public Tnode getFirstChild() { return firstChild; }

   /**
    * Set sibling.
    *
    * @param sibling - Node to set as sibling.
    */
   private void setRightSibling(Tnode sibling) { rightSibling = sibling; }

   /**
    * Get right sibling.
    *
    * @return - Node that is set as rightSibling.
    */
   public Tnode getRightSibling() { return rightSibling; }

   /**
    * Set info as integer. Used for calculations so there is no need to parse from string to int.
    *
    * @param i - String representation of a node.
    */
   private void setInfo (int i){ info = i; }

   /**
    * Get info.
    *
    * @return - Integer representation of info.
    */
   public int getInfo() { return info; }

   /**
    * Is node a leaf.
    *
    * @return - boolean if node is last in line or not.
    */
   public boolean isLeaf() { return (getFirstChild() == null); }

   @Override
   public String toString() {

      if (this.name == null)
         return "Node name is NULL and therefore can not be expressed as string";

      return leftParentheticRepresentation();
   }

   /**
    * Create a string that is left parenthetic representation of a tree.
    *
    * @return - string representation of a tree
    */
   private String leftParentheticRepresentation() {

      if (this.isLeaf()){
         return getName();
      }

      StringBuffer b = new StringBuffer();
      Stack<Tnode> stackToIterateOverTree = new Stack<>();
      stackToIterateOverTree.push(this);

      while (!stackToIterateOverTree.empty()){

         Tnode lowestElementInStack = stackToIterateOverTree.pop();
         b.append(lowestElementInStack.getName());

         if (lowestElementInStack.getRightSibling() != null) {
            stackToIterateOverTree.push(lowestElementInStack.getRightSibling());
         }

         if (lowestElementInStack.isLeaf()){
            if (lowestElementInStack.getRightSibling() != null){
               b.append(",");
            } else {
               if (!stackToIterateOverTree.empty()) {
                  b.append(")");
                  b.append(",");
               } else {
                  b.append(")");
               }
            }
         } else {
            stackToIterateOverTree.push(lowestElementInStack.getFirstChild());
            b.append("(");
         }
      }

      if (!this.getFirstChild().isLeaf() || !this.getFirstChild().getRightSibling().isLeaf()){
         b.append(")");
      }

      return b.toString();
   }

   /**
    * Create a tree from Reverse Polish Notation.
    *
    * @param pol - String representation of reverse polish notation arithmetic expression.
    * @return - Node that is made from RPN.
    */
   public static Tnode buildFromRPN (String pol) {

      if (pol == null || pol.isEmpty() || pol.trim().isEmpty())
         throw new NumberFormatException("Arithmetic expression is empty or null");

      String[] polListOfitems = pol.split(" ");
      Tnode root = new Tnode(polListOfitems[polListOfitems.length - 1], null, null);
      Stack<Tnode> stackOfLeaves = new Stack<>();

      ArrayList<String> operations = new ArrayList<>();

      operations.add("+");
      operations.add("-");
      operations.add("/");
      operations.add("*");
      operations.add("DUP");
      operations.add("SWAP");
      operations.add("ROT");

      for (int i = 0; i < polListOfitems.length - 1; i++){
         try{
            int isInt = Integer.parseInt(polListOfitems[i]);
            Tnode leaf = new Tnode("", null, null);
            leaf.setName(polListOfitems[i]);
            leaf.setInfo(isInt);
            stackOfLeaves.push(leaf);

         } catch (IllegalArgumentException e){
            if (operations.contains(polListOfitems[i])){

                if (polListOfitems[i].equals("DUP")){
                   if (stackOfLeaves.empty()){
                      throw new RuntimeException("No tree found to duplicate in: " + pol + "at " + polListOfitems[i]);
                   }
                   Tnode tnodeToDuplicate =  stackOfLeaves.peek();
                   // Deep copy
                   Tnode tnodeToPutInStack = new Tnode("", null, null);
                   tnodeToPutInStack.setName(tnodeToDuplicate.getName());
                   tnodeToPutInStack.setInfo(tnodeToDuplicate.getInfo());
                   tnodeToPutInStack.setFirstChild(tnodeToDuplicate.getFirstChild());
                   tnodeToPutInStack.setRightSibling(tnodeToDuplicate.getRightSibling());

                   stackOfLeaves.push(tnodeToPutInStack);

                } else if(polListOfitems[i].equals("SWAP")){

                   if (stackOfLeaves.size() < 2){
                      throw new RuntimeException("Not enough trees to swap in pol:" + pol + "at " + polListOfitems[i] + ". Two tree nodes are required!");
                   }
                    Tnode firstChild = stackOfLeaves.pop();
                    Tnode firstChildRightSibling = stackOfLeaves.pop();

                    stackOfLeaves.push(firstChild);
                    stackOfLeaves.push(firstChildRightSibling);
                } else if(polListOfitems[i].equals("ROT")){
                   if (stackOfLeaves.size() < 3){
                      throw new RuntimeException("Not enough trees to rotate in pol:" + pol + "at " + polListOfitems[i] + ". Three tree nodes are required!");
                   }
                    Tnode thirdTnode = stackOfLeaves.pop();
                    Tnode secondTnode = stackOfLeaves.pop();
                    Tnode firstTnode = stackOfLeaves.pop();

                    stackOfLeaves.push(secondTnode);
                    stackOfLeaves.push(thirdTnode);
                    stackOfLeaves.push(firstTnode);

                } else{
                    Tnode operation = new Tnode("", null, null);
                    operation.setName(polListOfitems[i]);

                    Tnode rightSibling = stackOfLeaves.pop();
                    Tnode firstChild = stackOfLeaves.pop();

                    operation.setFirstChild(firstChild);
                    operation.getFirstChild().setRightSibling(rightSibling);

                    stackOfLeaves.push(operation);
                }
            } else throw new RuntimeException("Argument: " + polListOfitems[i] + " Not an arithmetic expression in " + pol);
         }
      }

      if (pol.length() == 1){
         try{
            int isInt = Integer.parseInt(pol);
            root = new Tnode(pol, null, null);
            root.setInfo(isInt);
         } catch (IllegalArgumentException e){
            throw new RuntimeException("Tree can not be just an arithmetic expression in: " + pol);
         }
      } else {
            Tnode rootRightSibling = stackOfLeaves.pop();
            Tnode rootFirstChild = stackOfLeaves.pop();
            root.setFirstChild(rootFirstChild);
            root.getFirstChild().setRightSibling(rootRightSibling);
      }
      if (!stackOfLeaves.empty()){
         throw new RuntimeException("Too many numbers in expression: " + pol);
      }
      return root;
   }

   public static void main (String[] param) {
      String rpn = "2 1 - 4 * 6 3 / +";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN(rpn);
      System.out.println(res.toString());

      String rpn4 = "2 5 DUP ROT - + DUP *";
      System.out.println ("RPN: " + rpn4);
      Tnode res4 = buildFromRPN(rpn4);
      System.out.println(res4.toString());

      String rpn5 = "-3 -5 -7 ROT - SWAP DUP * +";
      System.out.println ("RPN: " + rpn5);
      Tnode res5 = buildFromRPN(rpn5);
      System.out.println(res5);

   }
}

